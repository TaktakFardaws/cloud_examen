const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;
app.use(cors());
app.use(bodyParser.json());

const db = mysql.createConnection({
  host: 'examen-db.cx2wke8og6f9.us-east-1.rds.amazonaws.com',
  user: 'admin',
  password: 'admin123',
  database: 'examendb',
});

db.connect((err) => {
  if (err) {
    console.error('Error connecting to the database:', err);
    return;
  }
  console.log('Connected to the database');
});

// Define a route to fetch data from the database
app.get('/data', (req, res) => {
  const query = 'SELECT * FROM exament'; 
  db.query(query, (err, result) => {
    if (err) {
      console.error('Error executing query:', err);
      res.status(500).send('Internal Server Error');
      return;
    }
    res.json(result);
  });
});

// Define a route to handle POST requests for submitting data
app.post('/submit', (req, res) => {
  const { name, description } = req.body;

  // Validate the presence of required fields
  if (!name || !description) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  const query = 'INSERT INTO exament (name, description) VALUES (?, ?)';
  const values = [name, description];

  db.query(query, values, (err, result) => {
    if (err) {
      console.error('Error executing query:', err);
      return res.status(500).send('Internal Server Error');
    }

    res.send('Data submitted successfully!');
  });
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

